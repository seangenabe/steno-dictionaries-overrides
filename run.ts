#!/usr/bin/env -S deno run --allow-read --allow-write

import { basename, join } from "https://deno.land/std@0.113.0/path/mod.ts";
import { ensureDir } from "https://deno.land/std@0.113.0/fs/ensure_dir.ts";
import { expandGlob } from "https://deno.land/std@0.113.0/fs/expand_glob.ts";
import d from "https://deno.land/x/dirname@1.1.2/mod.ts";

const statuses = await Promise.all([
  Deno.permissions.request({ name: "read" }),
  Deno.permissions.request({ name: "write" }),
]);

if (statuses.some((status) => status.state === "denied")) {
  throw new Error("Permissions denied");
}

const lowercaseLetters = [
  "A*",
  "PW*",
  "KR*",
  "TK*",
  "*E",
  "TP*",
  "TKPW*",
  "H*",
  "*EU",
  "SKWR*",
  "K*",
  "HR*",
  "PH*",
  "TPH*",
  "O*",
  "P*",
  "KW*",
  "R*",
  "S*",
  "T*",
  "*U",
  "SR*",
  "W*",
  "KP*",
  "KWR*",
  "STKPW*",
];

const rightHandedLetters = [
  "A",
  "PW-",
  "KR-",
  "TK-",
  "E",
  "TP-",
  "TKPW-",
  "H-",
  "EU",
  "SKWR-",
  "K-",
  "HR-",
  "PH-",
  "TPH-",
  "O",
  "P",
  "KW-",
  "R-",
  "S-",
  "T-",
  "U",
  "SR-",
  "W",
  "KP-",
  "KWR-",
  "STKPW-",
];

const allLetters = [
  ...lowercaseLetters,
  ...lowercaseLetters.map((c) => `${c}P`),
];

async function filterFingerspelledEntries(
  filePath: string,
  outputFilePath = filePath,
) {
  const allLettersSet = new Set(allLetters);
  const rightHandedLettersSet = new Set(
    rightHandedLetters.map((c) => `${c}RBGS`),
  );

  const typeyTypeContents = await readTextFileIfExists(filePath);
  if (typeyTypeContents == null) {
    return;
  }
  const typeyTypeEntries = Object.entries(
    JSON.parse(typeyTypeContents) as Record<string, string>,
  );

  console.log(`${basename(filePath)}:`);

  const filtered = typeyTypeEntries.filter(([stroke]) => {
    const strokeComponents = stroke.split("/");
    if (
      strokeComponents.every((component) => allLettersSet.has(component)) ||
      strokeComponents.every((component) =>
        rightHandedLettersSet.has(component)
      )
    ) {
      console.log(`Filtered out ${stroke}`);
      return false;
    }
    return true;
  });

  await Deno.writeTextFile(
    outputFilePath,
    JSON.stringify(Object.fromEntries(filtered), null, 2),
  );
}

if (import.meta.main) {
  const { __dirname } = d(import.meta);
  await ensureDir(join(__dirname, "dictionaries"));
  for await (
    const { path } of expandGlob("*.json", {
      root: join(__dirname, "steno-dictionaries", "dictionaries"),
    })
  ) {
    await Deno.copyFile(path, join(__dirname, "dictionaries", basename(path)));
  }
  for (
    const path of [
      "typey-type.json",
      "abbreviations.json",
      "code.json",
      "condensed-strokes.json",
      "condensed-strokes-fingerspelled.json",
      "currency.json",
      "dict-en-AU-with-extra-stroke.json",
      "dict.json",
      "misstrokes.json",
      "nouns.json",
      "top-1000-words.json",
      "top-10000-project-gutenberg-words.json",
    ]
  ) {
    await filterFingerspelledEntries(join(__dirname, "dictionaries", path));
  }
}

async function readTextFileIfExists(filePath: string) {
  try {
    return await Deno.readTextFile(filePath);
  } catch (err) {
    if (err instanceof Deno.errors.NotFound) {
      return null;
    }
    throw err;
  }
}
